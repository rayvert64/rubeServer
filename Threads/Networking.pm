package Threads::Networking;
#================================================================--
# File Name    : Networking.pm
#
# Purpose      : The Networking thread
#
# Author       : Philippe Boutin, Vancouver Island University
#
# System       : Perl (Linux)
#
#=================================================================
$| = 1;
use warnings;
use strict;

# Networking thread needs to have the config file 
sub thread1 {
    my $criticalVar = shift @_;
    my $criticalVarSms = shift @_;

    my @clients;

    my $buff;
    my $inbuff;
    my $soc = undef;
    my $newSoc= undef;
    my $select = IO::Select->new();
    my $time;

    # Here we try and open a socket on port defined in the config file
    my $port = Sockets::SocConS->new("localhost", 
                                    $criticalVar->{config}->{port},
                                    $criticalVar->{line},
                                    $criticalVarSms->{line},
                                    $criticalVarSms->{debugFh});

    while(1) {
        $port->read();
        $port->write();
    }
}

1;