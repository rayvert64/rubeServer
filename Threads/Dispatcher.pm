package Threads::Dispatcher;
#================================================================--
# File Name    : Networking.pm
#
# Purpose      : The Networking thread
#
# Author       : Philippe Boutin, Vancouver Island University
#
# System       : Perl (Linux)
#
#=================================================================
$| = 1;
use warnings;
use strict;

sub thread1 {
    my $criticalVar = shift @_;
    my $criticalVarSms = shift @_;
    my $controlFlowSms = shift @_;

    my $done = 0;
    my $rawpkt = "";
    my $wait = 0;
    my $turn = 0;
    my $fail = 0;
    my $time;

    my $rpkt = Packet::RubeG->new();
    $rpkt->set_opcode(9);
    $rpkt->set_msg("START");
    $rpkt->set_src_mac(ServerConfiguration::SOURCEMAC);

    # This is where the thread blocks untill someone registers
    $controlFlowSms->{dispatcher}->down();
    while ($done == 0) {
        # Check who is next
        $criticalVarSms->{nextSkip}->down();
        $turn = $criticalVar->{next};
        $criticalVarSms->{nextSkip}->up();

        # Deque next packet
        $criticalVarSms->{queue}->down();
        $rawpkt = $criticalVar->{queue}->dequeue();
        $criticalVarSms->{queue}->up();

        $rpkt->decode($rawpkt);

        if ($criticalVar->{next} == 0) {
            $wait = $criticalVar->{registrationtime};
        } else {
            $wait = ServerConfiguration::RUBETIMEOUT;
        }

        # Wait to send packet to rube goldberg machine
        if (!$controlFlowSms->{start}->down_timed($wait) && $criticalVar->{next} != 0) {
            $fail = 1;
        }

        if ($turn == 0) {
            $criticalVar->{startTime} = Time::HiRes::clock_gettime(Time::HiRes->CLOCK_MONOTONIC);
        }

        # If the packet we have was deleted we will know by checking 
        # Once we got the semaphore is down send go message as fast as possible
        $criticalVarSms->{nextSkip}->down();
        #   who is supposed to go next
        if (!$criticalVar->{skip}) {
            $criticalVar->{next}++;
            # Send packet and save the timestamp of when we sent it
            #    to the network layer.
            $criticalVarSms->{line}->down();
            $time = Time::HiRes::clock_gettime(Time::HiRes->CLOCK_MONOTONIC);
            $criticalVar->{line}->enqueue_packet($rawpkt);
            $criticalVarSms->{line}->up();
            
            $criticalVarSms->{table}->down();
            $criticalVar->{table}->set_serverSend(
                $rpkt->get_dest_mac(),
                $time    
            );
            $criticalVarSms->{table}->up();
            # Print Network debugging info to file
            if (ServerConfiguration::DEBUG) {
                $criticalVarSms->{debugFh}->down();
                Utils->print_debug("OUT: $time:\t$rawpkt\n");
                $criticalVarSms->{debugFh}->up();
            }

            # if previous one failed take note of it
            if ($fail) {
                $fail = 0;
                $criticalVarSms->{table}->down();
                my $failed = $criticalVar->{table}->find_turn($criticalVar->{next}-2);
                $criticalVar->{table}->set_total($failed, -1);
                $criticalVar->{table}->set_serverReceive($failed, $time);
                $criticalVarSms->{table}->up();
            }

            Collection::RubeUtils::sendTTL($criticalVar, $criticalVarSms);
        } elsif($criticalVar->{skip}) {
            $criticalVar->{skip} = 0;
        }
        $criticalVarSms->{nextSkip}->up();


        # Tell anyone waiting on dispatcher to go
        $controlFlowSms->{receiver}->up();

        # Run dispatcher and wait for the timeout of last client
        $criticalVarSms->{table}->down();
        if ($criticalVar->{next} > $criticalVar->{table}->get_max()-1) {
            $criticalVarSms->{table}->up();
            $done = 1;
            print("Dispatcher done!\n");
            # Wait to send packet to rube goldberg machine
            if (!$controlFlowSms->{start}->down_timed(ServerConfiguration::RUBETIMEOUT)) {
                $criticalVarSms->{table}->down();
                my $failed = $criticalVar->{table}->find_turn($criticalVar->{next}-1);
                $criticalVar->{table}->set_total($failed, -1);
                $criticalVar->{table}->set_serverReceive($failed, Time::HiRes::clock_gettime(Time::HiRes->CLOCK_MONOTONIC));
                $criticalVarSms->{table}->up();
            }

            Collection::RubeUtils::sendRunTime($criticalVar, $criticalVarSms, $controlFlowSms);
        }
        $criticalVarSms->{table}->up();
    }
}

1;