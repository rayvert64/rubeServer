package Threads::Receiver;
#================================================================--
# File Name    : Receiver.pm
#
# Purpose      : The Receiver thread
#
# Author       : Philippe Boutin, Vancouver Island University
#
# System       : Perl (Linux)
#
#=================================================================
$| = 1;
use warnings;
use strict;

sub thread1 {
    my $criticalVar = shift @_;
    my $criticalVarSms = shift @_;
    my $controlFlowSms = shift @_;

    # Create file handle:
    my $debugFh = FileHandle->new();

    my $inq = undef;
    my $time;
    my $timestamp;
    my $rpkt = Packet::RubeG->new();
    my $key;
    my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst); 

    while (1) {
        $inq = undef;
        
        $criticalVarSms->{line}->down();
        $inq = $criticalVar->{line}->dequeue_packet();
        
        $timestamp = Time::HiRes::clock_gettime(Time::HiRes->CLOCK_MONOTONIC);
        $criticalVarSms->{line}->up();

        if (defined($inq) && $inq ne ServerConfiguration::HEARTBEAT) {
            $rpkt->decode($inq);
            $criticalVarSms->{table}->down();

            my $val = $rpkt->get_msg();
            my $src_mac = $rpkt->get_src_mac();
            
            my $turn = $criticalVar->{table}->get_turn($src_mac);
            
            my $opcode = $rpkt->get_opcode();
            if ($opcode == 0) {
                $rpkt->set_opcode(1);
                $rpkt->set_dest_mac($src_mac);
                $rpkt->set_src_mac(ServerConfiguration::SOURCEMAC);
                $rpkt->set_msg("PINGRESPONSE");
                
                my $raw = $rpkt->encode();

                $criticalVarSms->{line}->down();
                $time = Collection::Utils->get_time();
                $criticalVar->{line}->enqueue_packet($raw);
                $criticalVarSms->{line}->up();
                
                if (ServerConfiguration::DEBUG) {
                    $criticalVarSms->{debugFh}->down();
                    Utils->print_debug("OUT: $time:\t$raw\n");
                    $criticalVarSms->{debugFh}->up();
                }
            } elsif ($opcode == 2 && $val eq "REGISTRATION") {
                    # If we already started don't take registrations
                    if (!defined($turn) && $criticalVar->{startTime} == 0) {
                        Collection::RubeUtils::register($criticalVar, 
                                                        $criticalVarSms, 
                                                        $controlFlowSms, 
                                                        $src_mac, $rpkt);
                    } elsif (defined($turn)) {
                        # This function also has the function of giving the remaining time 
                        # before start if the client is already registered
                        Collection::RubeUtils::register($criticalVar, 
                                                        $criticalVarSms, 
                                                        $controlFlowSms, 
                                                        $src_mac, $rpkt, $turn);
                    }
            } elsif ($opcode == 4 && $val eq "UNREGISTER" && defined($turn)) {
                if ($criticalVar->{startTime} == 0) {
                    Collection::RubeUtils::unregister($criticalVar, 
                                                      $criticalVarSms, 
                                                      $controlFlowSms,
                                                      $src_mac, $turn, $rpkt);
                }
            } elsif ($opcode == 8 && defined($turn) && $turn == ($criticalVar->{next}-1)) {
                # Check if time was already assigned to that address
                if (!defined($criticalVar->{table}->get_total($src_mac))) {
                    $criticalVar->{table}->set_serverReceive(
                        $src_mac,
                        $timestamp
                    );
                    
                    #unlock table for dispatcher and let dispatcher run
                    $criticalVarSms->{table}->up();
                    $controlFlowSms->{start}->up();

                    $controlFlowSms->{receiver}->down();

                    # Set total time, the message value should just be a number
                    print("$src_mac took $val seconds to complete.\n");

                    $criticalVar->{table}->set_total($src_mac, $val);
                    
                    # Unclock the semaphore so that the other Rube goldberg machine can start 
                    #    right away.
                    
                    if ($turn == $criticalVar->{table}->get_max()-1) {
                        $criticalVar->{endTime} = Time::HiRes::clock_gettime(Time::HiRes->CLOCK_MONOTONIC);
                    }
                }
            }
            $criticalVarSms->{table}->up();
        }
    }
}

1;