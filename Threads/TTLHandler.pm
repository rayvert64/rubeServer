package Threads::TTLHandler;
#================================================================--
# File Name    : TTLHandler.pm
#
# Purpose      : The Table time decrementer
#
# Author       : Philippe Boutin, Vancouver Island University
#
# System       : Perl (Linux)
#
#=================================================================
sub thread1 {
    my $criticalVar = shift @_;
    my $criticalVarSms = shift @_;
    my $controlFlowSms = shift @_;

    while (1) {
        # We only want to run this once every second, so we block on every loop.
        #   Since this is not a binary semaphore, we can actually run this more 
        #       than once if the server was being overloaded at some point.  
        $controlFlowSms->{ttl}->down();
        $criticalVarSms->{table}->down();
        $criticalVar->{table}->dec_ttl();
        $criticalVarSms->{table}->up();
    }
}

1;