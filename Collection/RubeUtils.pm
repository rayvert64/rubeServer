package Collection::RubeUtils;
#================================================================--
# File Name    : RubeUtils.pm
#
# Purpose      : The Utility functions for the rube server that 
#                   can be used by many threads
#
# Author       : Philippe Boutin, Vancouver Island University
#
# System       : Perl (Linux)
#
# TODO         : Make this not a single 500-line monstrosity of 
#                   a file
#
#=================================================================
$| = 1;
use warnings;
use strict;

sub sendTTL {
    my $criticalVar = shift @_;
    my $criticalVarSms = shift @_;

    my $rpkt = Packet::RubeG->new();
    my $raw = 0;
    my $myTime = 0;
    my $timestamp = 0;

    my @keys = $criticalVar->{table}->get_keys();
    foreach my $key (@keys) {
        my $myturn = $criticalVar->{table}->get_turn($key);
        if ($myturn >= $criticalVar->{next}) {
            $criticalVar->{table}->set_ttl($key, ($myturn-$criticalVar->{next}+1)*ServerConfiguration::RUBETIMEOUT);

            # send time to rube goldberg machine
            $rpkt->set_opcode(3);
            $rpkt->set_msg($criticalVar->{table}->get_ttl($key));
            $rpkt->set_src_mac(ServerConfiguration::SOURCEMAC);
            $rpkt->set_dest_mac($key);

            $raw = $rpkt->encode();

            $criticalVarSms->{line}->down();
            $timestamp = Collection::Utils->get_time();
            $criticalVar->{line}->enqueue_packet($raw);
            $criticalVarSms->{line}->up();
            
            if (ServerConfiguration::DEBUG) {
                $criticalVarSms->{debugFh}->down();
                Utils->print_debug("OUT: $timestamp:\t$raw\n");
                $criticalVarSms->{debugFh}->up();
            }
        }
    }
}

sub sendRunTime {
    my $criticalVar = shift @_;
    my $criticalVarSms = shift @_;
    my $controlFlowSms = shift @_;

    my $fh = FileHandle::new();

    # Build filename 
    my $filename = ServerConfiguration::FILENAME;

    # Build stats file and create filehandle, then send it to all clients
    $fh->open("> $filename");

    # Write to file the contents of the table
    my ($fail,$ASCIITable) = $criticalVar->{table}->write_summary();

    print $fh "Summary - Rube Goldberg Machine\nDate: ".ServerConfiguration::LOCALTIME."\n";
    print $fh "Total Time = ".($criticalVar->{endTime}-$criticalVar->{startTime})."\n";
    if ($fail) {
        print $fh "##############FAILED##############\n\n";
    } else {
        print $fh "\n";
    }

    print($ASCIITable->draw( ['.=','=.','-','-'],   # .=-----------=.
                                 ['|','|','|'],         # | info | info |
                                 ['|-','-|','=','='],   # |-===========-|
                                 ['|','|','|'],         # | info | info |
                                 ["'=","='",'-','-'],   # '=-----------='
                                 ['|=','=|','-','+']    # rowseperator;
    ));

    print $fh $ASCIITable->draw( ['.=','=.','-','-'],   # .=-----------=.
                                 ['|','|','|'],         # | info | info |
                                 ['|-','-|','=','='],   # |-===========-|
                                 ['|','|','|'],         # | info | info |
                                 ["'=","='",'-','-'],   # '=-----------='
                                 ['|=','=|','-','+']    # rowseperator;
    );

    # Close file handle
    close($fh);

    $controlFlowSms->{quit} = 1;
}

sub register {
    my $criticalVar = shift @_;
    my $criticalVarSms = shift @_;
    my $controlFlowSms = shift @_;

    my $src_mac = shift @_;
    my $rpkt = shift @_;
    my $turn = shift @_;

    my $raw = undef;
    my $max = $criticalVar->{table}->get_max();
    my $ttl = $criticalVar->{registrationtime};
    my $time;

    $rpkt->set_dest_mac($src_mac);

    # Add client or get clients' ttl
    if ($max == 0 && !defined($turn)) {
        $criticalVar->{table}->add($src_mac, $ttl);
    } elsif ($max !=  0 && !defined($turn)) {
        $ttl = ($criticalVar->{table}->get_ttl($criticalVar->{table}->find_turn(0)))+($max*ServerConfiguration::RUBETIMEOUT);
        $criticalVar->{table}->add($src_mac, $ttl);
    } else {
        $ttl = $criticalVar->{table}->get_ttl($src_mac);
    }

    print("REG: $src_mac is registered!\n");

    # Change message to start
    $rpkt->set_msg("START");
    $rpkt->set_src_mac(ServerConfiguration::SOURCEMAC);
    $rpkt->set_opcode(9);

    # send packet to dispatcher
    $criticalVarSms->{queue}->down();
    $criticalVar->{queue}->enqueue($rpkt->encode());
    $criticalVarSms->{queue}->up();

    # Send time to rube goldberg machine
    $rpkt->set_opcode(3);
    $rpkt->set_msg($ttl);

    $raw = $rpkt->encode();

    # Enqueue packet to network
    $criticalVarSms->{line}->down();
    $time = Collection::Utils->get_time();
    $criticalVar->{line}->enqueue_packet($raw);
    $criticalVarSms->{line}->up();
    
    if (ServerConfiguration::DEBUG) {
        $criticalVarSms->{debugFh}->down();
        Utils->print_debug("OUT: $time:\t$raw\n");
        $criticalVarSms->{debugFh}->up();
    }

    # Signal thread 3 to start.
    $controlFlowSms->{dispatcher}->up();
}

sub unregister {
    my $criticalVar = shift @_;
    my $criticalVarSms = shift @_;
    my $controlFlowSms = shift @_;

    my $src_mac = shift @_;
    my $turn = shift @_;
    my $rpkt = shift @_;
    $rpkt->set_src_mac(ServerConfiguration::SOURCEMAC);
    
    my $newTime = 0;
    my $raw = undef;

    # Check whether or not the person who unregistered is the one to go next
    if ($turn == $criticalVar->{next}) {
        # If it is tell dispatcher to skip
        $criticalVarSms->{nextSkip}->down();
        $criticalVar->{skip} = 1;
        $criticalVarSms->{nextSkip}->up();
        $controlFlowSms->{start}->up();
    } else {
        $criticalVarSms->{queue}->down();
        $criticalVar->{queue}->purge_after($turn-$criticalVar->{next}-1);
        $criticalVarSms->{queue}->up();
    }

    my $maxTurn = $criticalVar->{table}->get_max();

    my $timePrev = $criticalVar->{table}->get_ttl($src_mac);

    for (my $i = $turn+1; $i < $maxTurn; $i++) {
        my $key = $criticalVar->{table}->find_turn($i);
        my $time = $timePrev;
        $timePrev = $criticalVar->{table}->get_ttl($key);
        $criticalVar->{table}->set_ttl($key, $time);

        # send time to rube goldberg machine
        $rpkt->set_dest_mac($key);

        # Change message to start
        $rpkt->set_opcode(9);
        $rpkt->set_msg("START");

        # send packet to dispatcher
        $criticalVarSms->{queue}->down();
        $criticalVar->{queue}->enqueue($rpkt->encode());
        $criticalVarSms->{queue}->up();

        $rpkt->set_opcode(3);
        $rpkt->set_msg($time);
        $raw = $rpkt->encode();

        $criticalVarSms->{line}->down();
        $time = Collection::Utils->get_time();
        $criticalVar->{line}->enqueue_packet($raw);
        $criticalVarSms->{line}->up();
        
        if (ServerConfiguration::DEBUG) {
            $criticalVarSms->{debugFh}->down();
            Utils->print_debug("OUT: $time:\t$raw\n");
            $criticalVarSms->{debugFh}->up();
        }
    }
    
    # Delete table entry
    $criticalVar->{table}->delete($src_mac);

    # Send unregistration confirmation to unregistered client
    $rpkt->set_dest_mac($src_mac);
    $rpkt->set_opcode(5);
    $rpkt->set_msg("UNREGISTERED");
    $raw = $rpkt->encode();

    $criticalVarSms->{line}->down();
    my $time = Collection::Utils->get_time();
    $criticalVar->{line}->enqueue_packet($raw);
    $criticalVarSms->{line}->up();
    
    if (ServerConfiguration::DEBUG) {
        $criticalVarSms->{debugFh}->down();
        Utils->print_debug("OUT: $time:\t$raw\n");
        $criticalVarSms->{debugFh}->up();
    }
}