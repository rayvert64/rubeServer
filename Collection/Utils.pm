package Collection::Utils;
#================================================================--
# File Name    : Collection::utils.pm
#
# Purpose      : implements Server utilities
#
# Author       : Philippe Boutin, Vancouver Island University
#
# System       : Perl (Linux)
#
#=========================================================

$| = 1;
use strict;
use warnings;

# Create file handle:
my $debugFh = FileHandle->new();

sub get_time {
    my ($epch, $uepch) = Time::HiRes::gettimeofday();

    my $string = sprintf("%s.%05d", POSIX::strftime("%H:%M:%S", localtime($epch)), $uepch/10);

    return $string;
}

sub print_debug {
    my $pkg = shift @_;
    my $string = shift @_;

    print($string);
    #$debugFh->open(">> ".ServerConfiguration::DEBUGFILENAME);
    #print $debugFh "OUT: $time:\t$raw\n";
    #$debugFh->close();
}

1;