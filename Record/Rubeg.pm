package Record::Rubeg;
#================================================================--
# File Name    : Record/Rubeg.pm
#
# Purpose      : implements Rubeg record
#
# Author       : Philippe Boutin, Vancouver Island University
#
# System       : Perl (Linux)
#
#=========================================================

$| = 1;
use strict;
use warnings;

my $ttl = 0;
my $turn = 0;
my $total = undef;
my $serverSend = 0;
my $serverReceive = 0;
my $start = 0;

sub  new {
   my $class = shift @_;

   my $self = {
       turn => $turn,
       ttl => $ttl,
       total => $total,
       serverSend => $serverSend,
       serverReceive => $serverReceive,
       start => $start
   };
     
   bless ($self, $class);
   return $self;
}

sub get_total {
   my $self = shift @_;
   
   return $self->{total};
}

sub set_total {
   my $self = shift @_;
   my $t = shift @_;
 
   $self->{total} = $t;
   return;
}

sub get_turn {
   my $self = shift @_;
   
   return $self->{turn};
}

sub set_turn {
   my $self = shift @_;
   my $t = shift @_;
 
   $self->{turn} = $t;
   return;
}

sub get_ttl {
   my $self = shift @_;
   
   return $self->{ttl};
}

sub set_ttl {
   my $self = shift @_;
   my $t = shift @_;
 
   $self->{ttl} = $t;
   return;
}

sub get_serverSend {
   my $self = shift @_;
   
   return $self->{serverSend};
}

sub set_serverSend {
   my $self = shift @_;
   my $t = shift @_;
 
   $self->{serverSend} = $t;
   return;
}

sub get_serverReceive {
   my $self = shift @_;
   
   return $self->{serverReceive};
}

sub set_serverReceive {
   my $self = shift @_;
   my $t = shift @_;
 
   $self->{serverReceive} = $t;
   return;
}

sub start {
   my $self = shift @_;

   $self->{start} = 1;
}

sub stop {
   my $self = shift @_;

   $self->{start} = 0;
}

sub is_started {
   my $self = shift @_;

   return $self->{start};
}

sub dumps {
   my $self = shift @_;

   my $s = '';

   $s = $s . "Rubeg: $self->{turn}  TTL: $self->{ttl}";

   return $s;
}

sub dump {
   my $self = shift @_;

   print ("Rubeg: ".$self->{turn}." TTL: ".$self->{ttl}."\n");
   
   return;
}

1;
