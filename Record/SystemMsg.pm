package Ruth::Record::SystemMsg;
#================================================================--
# File Name    : Record/SystemMsg.pm
#
# Purpose      : implements System messages record
#
# Author       : Philippe Boutin , Vancouver Island University
#
# System       : Perl (Linux)
#
#=========================================================

$| = 1;
use strict;
use warnings;


my $bootupFlag = 0;
my $requestFlag = 0;

sub  new {
   my $class = shift @_;

   my $self = {
      mqueue => Tosf::Collection::Queue->new(),
   };
                
   bless ($self, $class);
   return $self;
}

sub enqueue {
   my $self = shift @_;
   my $p = shift @_;
   
   $self->{mqueue}->enqueue($p);
}

sub get_num_msg {
   my $self = shift @_;
   
   return $self->{mqueue}->get_siz();
}

sub dequeue {
   my $self = shift @_;
   
   return ($self->{mqueue}->dequeue());
}

sub dumps {
   my $self = shift @_;

   my $s = '';

   $s = $s . "Queue Size: " . $self->{mqueue}->get_siz();
   $s = $s . "\n\t" . $self->{mqueue}->dumps();

   return $s;
}


sub dump {
   my $self = shift @_;

   print ("Name: $self->{name}");
   $self->{mqueue}->dump();
   return;
}

1;
