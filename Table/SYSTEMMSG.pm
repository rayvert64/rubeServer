package Ruth::Table::SYSTEMMSG;
#================================================================--
# File Name    : Table/SYSTEMMSG.pm
#
# Purpose      : table of system messages records
#
# Author       : Philippe Boutin, Vancouver Island University
#
# System       : Perl (Linux)
#
#=========================================================

$| = 1;
use strict;
use warnings;

my %table;

# FSM name primary key
#
sub get_keys {

   return keys(%table);
}

sub add {
   my $pkg = shift @_;
   my %params = @_;

   if (!defined($params{name})) {
      die(Tosf::Exception::Trap->new(name => "Ruth::Table::SYSTEMMSG->add"));
   }

   my $name = $params{name};

   if (!exists($table{$name})) {
      $table{$name} = Ruth::Record::SystemMsg->new();
   }

}

sub delete {
   my $pkg = shift @_;
   my $name = shift @_; #primary key

   if (!defined($name)) {
      die(Tosf::Exception::Trap->new(name => "Ruth::Table::SYSTEMMSG->delete"));
   }

   if (!exists($table{$name})) {
      die(Tosf::Exception::Trap->new(name => "Ruth::Table::SYSTEMMSG->delete"));
   }

   delete($table{$name});

}

sub get_size {
   my $pkg = shift @_;
   my $name = shift @_;

   if (!defined($name)) {
      die(Tosf::Exception::Trap->new(name => "Ruth::Table::SYSTEMMSG->get_siz"));
   }

   if (exists($table{$name})) {
      return $table{$name}->get_siz();
   } else {
      return undef;
   } 
}

sub enqueue {
   my $pkg = shift @_;
   my $name = shift @_;
   my $m = shift @_;

   if (!defined($name) || (!defined($m))) {
      die(Tosf::Exception::Trap->new(name => "Ruth::Table::SYSTEMMSG->enqueue"));
   }

   if (!exists($table{$name})) {
      $table{$name} = Ruth::Record::SystemMsg->new();
   } 

   $table{$name}->enqueue($m);
}

sub dequeue {
   my $pkg = shift @_;
   my $name = shift @_;

   if (!defined($name)) {
      die(Tosf::Exception::Trap->new(name => "Ruth::Table::SYSTEMMSG->dequeue"));
   }

   if (exists($table{$name})) {
      return $table{$name}->dequeue();
   } else {
      return undef;
   } 
}

sub dumps {
   my $self = shift @_;

   my $key;
   my $s = '';

   foreach $key (keys(%table)) {
      $s = $s . "Thr: $key\t";
      $s = $s . $table{$key}->dumps();
      $s = $s . "\n";
   } 
   return $s;
}

sub dump {
   my $self = shift @_;

   my $key;

   foreach $key (keys(%table)) {
      print ("Thr: $key \n");
      $table{$key}->dump();
      print ("\n");
   } 
}

1;
