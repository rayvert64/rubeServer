package Table::RUBEG;
#================================================================--
# File Name    : Table/RUBEG.pm
#
# Purpose      : table of RUBEG records
#
# Author       : Philippe Boutin, Vancouver Island University
#
# System       : Perl (Linux)
#
#=========================================================

$| = 1;
use strict;
use warnings;

sub new {
   my $class = shift @_;

   my $self = {
      table => {},
      max => 0
   };

   bless ($self, $class);
   return $self;
}

# Get the turn number of the last Rube Goldberg Machine
sub get_max {
   my $self = shift @_;

   return $self->{max};
}

sub delete {
   my $self = shift @_;
   my $mac = shift @_; #primary key
   my $key;
   my $time;
   my $otherTime;
   my $turn;
   my $otherTurn;

   if (!defined($mac)) {
      die("Table::RUBEG->delete");
   }

   if (!exists($self->{table}{$mac})) {
      die("Table::RUBEG->delete");
   }

   $time = $self->{table}{$mac}->get_ttl();
   $turn = $self->{table}{$mac}->get_turn();

   foreach $key (keys(%{$self->{table}})) {
      $otherTurn = $self->{table}{$key}->get_turn();
      if ($turn < $otherTurn) {
         $self->{table}{$key}->set_turn($otherTurn-1);
      }
   }

   delete($self->{table}{$mac});
   $self->{max} = $self->{max}-1;
}

sub get_keys {
   my $self = shift @_;

   return keys(%{$self->{table}});
}

sub add {
   my $self = shift @_;
   my $mac = shift @_; #primary key
   my $time = shift @_;

   if (!defined($mac) || (!defined($time))) {
      die("Table::RUBEG->add");
   }

   if (!exists($self->{table}{$mac})) {
      $self->{table}{$mac} = threads::shared::shared_clone(Record::Rubeg->new());
      $self->{table}{$mac}->set_turn($self->{max});
      $self->{table}{$mac}->set_ttl($time);
      $self->{max} = $self->{max} + 1;
   }

}

sub find_turn {
   my $self = shift @_;
   my $turn = shift @_;

   if (!defined($turn)) {
      die("Table::RUBEG->find_turn");
   }

   my $key;

   foreach $key (keys(%{$self->{table}})) {
      if ($self->{table}{$key}->get_turn() == $turn) {
         return $key;
      }
   }
   return undef;
}

sub get_turn {
   my $self = shift @_;
   my $mac = shift @_;

   if (!defined($mac)) {
      die("Table::RUBEG->get_turn");
   }

   if (exists($self->{table}{$mac})) {
      return $self->{table}{$mac}->get_turn();
   } else {
      return undef;
   } 
}

sub set_total {
   my $self = shift @_;
   my $mac = shift @_;
   my $t = shift @_;

   if (!defined($mac) || (!defined($t)) || (!exists($self->{table}{$mac})) ) {
      die("Table::RUBEG->set_total");
   }


   $self->{table}{$mac}->set_total($t);
}

sub get_total {
   my $self = shift @_;
   my $mac = shift @_;

   if (!defined($mac)) {
      die("Table::RUBEG->get_total");
   }
   if (exists($self->{table}{$mac})) {
      return $self->{table}{$mac}->get_total();
   } else {
      return undef;
   } 
}

sub set_ttl {
   my $self = shift @_;
   my $mac = shift @_;
   my $t = shift @_;

   if (!defined($mac) || (!defined($t)) ||  (!exists($self->{table}{$mac})) ) {
      die("Table::RUBEG->set_ttl");
   }


   $self->{table}{$mac}->set_ttl($t);
}

sub get_ttl {
   my $self = shift @_;
   my $mac = shift @_;

   if (!defined($mac)) {
      die("Table::RUBEG->get_ttl");
   }
   if (exists($self->{table}{$mac})) {
      return $self->{table}{$mac}->get_ttl();
   } else {
      return undef;
   } 
}

sub set_serverSend {
   my $self = shift @_;
   my $mac = shift @_;
   my $t = shift @_;

   if (!defined($mac) || (!defined($t)) ||  (!exists($self->{table}{$mac})) ) {
      die("Table::RUBEG->set_serverSend");
   }


   $self->{table}{$mac}->set_serverSend($t);
}

sub get_serverSend {
   my $self = shift @_;
   my $mac = shift @_;

   if (!defined($mac)) {
      die("Table::RUBEG->get_serverSend");
   }
   if (exists($self->{table}{$mac})) {
      return $self->{table}{$mac}->get_serverSend();
   } else {
      return undef;
   } 
}

sub set_serverReceive {
   my $self = shift @_;
   my $mac = shift @_;
   my $t = shift @_;

   if (!defined($mac) || (!defined($t)) ||  (!exists($self->{table}{$mac})) ) {
      die("Table::RUBEG->set_serverReceive");
   }


   $self->{table}{$mac}->set_serverReceive($t);
}

sub get_serverReceive {
   my $self = shift @_;
   my $mac = shift @_;

   if (!defined($mac)) {
      die("Table::RUBEG->get_serverReceive");
   }
   if (exists($self->{table}{$mac})) {
      return $self->{table}{$mac}->get_serverReceive();
   } else {
      return undef;
   } 
}

sub dec_ttl {
   my $self = shift @_;

   my $key;
   my $time;

   foreach $key (keys(%{$self->{table}})) {
      $time = $self->{table}{$key}->get_ttl();
      if ($time != 0) {
         $self->{table}{$key}->set_ttl($time-1);
      }
   } 
}

sub start {
   my $self = shift @_;
   my $mac = shift @_;

   if (exists($self->{table}{$mac})) {
      $self->{table}{$mac}->start();
   }
}

sub stop {
   my $self = shift @_;
   my $mac = shift @_;

   if (exists($self->{table}{$mac})) {
      $self->{table}{$mac}->stop();
   }
}

sub is_started {
   my $self = shift @_;
   my $mac = shift @_;

   if (exists($self->{table}{$mac})) {
      return $self->{table}{$mac}->is_started();
   } else {
      return undef;
   }
}

sub clear {
   my $self = shift @_;

   my $key;

   foreach $key (keys(%{$self->{table}})) {
      delete($self->{table}{$key});
      $self->{max} = $self->{max}-1;
   } 
}

sub write_summary {
   my $self = shift @_;

   my $table = Text::ASCIITable->new({headingText => 'Rube Goldberg Summary'});
   $table->setCols('Mac','Run Time', 'Aprox Latency');
   $table->setOptions('drawRowLine',1);

   my $fail = 0;

   my $key;

   my $totalTime = 0;
   my $totalLatency = 0;
   my $counter = 0; 

   my $time = 0;
   my $serverR = 0;
   my $serverS = 0;

   my $aprxLatency = 0;

   # Get the full time that the server ran after sending the first start packet and 
   #   receiving the last time packet.
   my $serverTime = $self->{table}{$self->find_turn($self->{max}-1)}->get_serverReceive();
   $serverTime -= $self->{table}{$self->find_turn(0)}->get_serverSend();


   # For each turn get the runtime of the rube goldberg
   #    machine and check to see if it failed
   for (my $turn = 0; $turn<$self->{max}; $turn++) {
      $key = $self->find_turn($turn);
      $time = $self->{table}{$key}->get_total();
      $serverS = $self->{table}{$key}->get_serverSend();
      $serverR = $self->{table}{$key}->get_serverReceive();

      # If we notice it failed then, mark the entire machine
      #    as failed.
      if (!defined($time) || $time == -1) {
         $time = "FAIL";
         $aprxLatency = "FAIL";
         $fail = 1;

         # Deduce the time from when we sent a packet to this one to 
         #    when the next start packet was sent to ignore this one's
         #    time.
         $serverTime -= $serverR-$serverS;
         print("Server receive($serverR) - Server Send($serverS) = ".($serverR-$serverS)."\nServer time = ".$serverTime."\n");
      
      # Else calculate the latency by grabing the send/receive time
      #     minus the actual time of the machine, all that divided by 2
      } else {
         $aprxLatency = ($serverR - $serverS - $time)/2;
         
         $totalTime += $time;
         $totalLatency += $aprxLatency;
         $counter++;
      }

      # Add row to table
      $table->addRow($key,
         $time,
         $aprxLatency
      );
      $table->addRowLine();
   }

   $table->addRowLine();
   if ($counter != 0) {
      $table->addRow('', 'Average Latency', $totalLatency/$counter);
   } else {
      $table->addRow('', 'Average Latency', 0);
   }
   
   if (int($serverTime) != 0) {
      $table->addRow('', 'Total client time', $totalTime);
      $table->addRow('', 'Total server time', $serverTime);
      $table->addRowLine();
      my $percentage = sprintf("%.2f", ((($serverTime-$totalTime)/$serverTime)*100));
      $table->addRow('', 'Percentage Latency time', $percentage."%");
   }

   # Return the status of the machine and the table to write to a file
   return ($fail,$table);
}

sub dumps {
   my $self = shift @_;

   my $key;
   my $s = '';

   foreach $key (keys(%{$self->{table}})) {
      $s = $s . "RUBEG entry: $key ";
      $s = $s . $self->{table}{$key}->dumps();
      $s = $s . "\n";
   } 
   return $s;
}

sub dump {
   my $self = shift @_;

   my $key;

   foreach $key (keys(%{$self->{table}})) {
      print ("Name: $key \n");
      $self->{table}{$key}->dump();
      print ("\n");
   } 
}

1;
