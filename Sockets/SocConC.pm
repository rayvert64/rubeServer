package Sockets::SocConC;
#================================================================--
# File Name    : Sockets::SocConC
#
# Purpose      : ServerSockets
#
# Author       : Philippe Boutin, Vancouver Island University
#
# System       : Perl (Linux)
#
#=========================================================

$| = 1;
use strict;
use warnings;

use constant MAXLEN => 100;
use constant HEARTBEAT => 'heartbeat';
use constant HEARTBEATPKT => '_G_heartbeat_H_';

sub new {
    my $class = shift @_;
    my $hostname = shift @_;
    my $port = shift @_;
    my $line = shift @_;

    my $self = {
        hostname => $hostname,
        port => $port,
        max => 0,
        counter => 0,
        buff => undef,
        inbuff => undef,
        soc => undef,
        select => IO::Select->new(),
        timeout => 15,
        timeoutTime => 0,
        heartBeatTimeout => 10,
        heartBeatTime => 0,
        line => $line,
    };

    bless ($self, $class);
    return $self;
}

sub connect {
    my $self = shift @_;

    # Here we try and open a socket on port 42069
    print("Try to open socket at host localhost / port ".$self->{port}.".\n");
    $self->{soc} = IO::Socket::INET->new(
        PeerAddr => $self->{hostname},
        PeerPort => $self->{port},
        Proto => 'tcp',
        Timeout  => 00001,
        Blocking => 0
    );

    if (defined($self->{soc})) {
        print("Opened interface at host localhost / port $self->{port} \n");
        $self->{select}->add($self->{soc});
    }

    $self->{timeoutTime} = Time::HiRes::clock_gettime(Time::HiRes->CLOCK_REALTIME);

    return 0;
}

sub connected {
    my $self = shift @_;

    if (defined($self->{soc})) {
        return 1;
    }

    return 0;
}

sub disconnect {
    my $self = shift @_;

    if (defined($self->{soc})) {
        if ($self->{select}->exists($self->{soc})) {
            $self->{select}->remove($self->{soc});
            print("Removed reference to interface from select \n");
        }
        print("Reseting interface \n");

        close($self->{soc});
    }

    $self->{timeoutTime} = Time::HiRes::clock_gettime(Time::HiRes->CLOCK_REALTIME);

    $self->{soc} = undef;
}

sub read {
    my $self = shift @_;

    if (defined($self->{soc})) {
        my $timeout = Time::HiRes::clock_gettime(Time::HiRes->CLOCK_REALTIME) -
                        $self->{timeoutTime};
        if ($self->{timeout} > $timeout) {
            $self->{buff} = undef;
            my @clients = $self->{select}->can_read(0);
            foreach my $fh (@clients) {
                if ($fh == $self->{soc}) {
                    $self->{soc}->recv($self->{buff}, MAXLEN);
                    last;
                }
            }

            if (defined($self->{buff}) && length($self->{buff}) != 0) {
                $self->{timeoutTime} = Time::HiRes::clock_gettime(Time::HiRes->CLOCK_REALTIME);
                if ($self->{buff} ne HEARTBEATPKT) {
                    $self->{line}->enqueue_packet_fragment($self->{buff});     
                }
            }
        } else {
            $self->disconnect();
        }
    } else {
        #print("Connect".$self->{port}."\n");
        $self->connect();
    }
}

sub write {
    my $self = shift @_;

    if (defined($self->{soc})) {
        $self->{buff} = $self->{line}->dequeue_packet_fragment();

        if (defined($self->{buff}) && defined($self->{soc})) {
            $self->{soc}->write($self->{buff});
        }

        my $timeout = Time::HiRes::clock_gettime(Time::HiRes->CLOCK_REALTIME) -
                        $self->{heartBeatTime};

        # After 10 secs we send a heartbeats
        if ($self->{heartBeatTimeout} < $timeout && defined($self->{soc})) {
            $self->{heartBeatTime} = Time::HiRes::clock_gettime(Time::HiRes->CLOCK_REALTIME);
            $self->{buff} = $self->{line}->enqueue_packet(HEARTBEAT);
        }
    } else {
        #print("Connect".$self->{port}."\n");
        $self->connect();
    }
}

1;