package ServerConfiguration;
#================================================================--
# File Name    : config.pm
#
# Purpose      : implements Server Settings
#
# Author       : Philippe Boutin, Vancouver Island University
#
# System       : Perl (Linux)
#
#=========================================================

$| = 1;
use strict;
use warnings;

use constant DEBUG => 0;
use constant MAXLEN => 100;
use constant HEARTBEAT => 'heartbeat';
use constant HEARTBEATPKT => '_G_heartbeat_H_';
use constant MINUTE => 60;
use constant REGISTRATIONTIME => 3*MINUTE;
use constant RUBETIMEOUT => MINUTE;
use constant SOURCEMAC => 0;
use constant LOCALTIME => POSIX::strftime("_%m-%d-%y\_%H-%M", localtime());
use constant FILENAME => 'log/results/RubeGSummary'.LOCALTIME.".txt";
use constant DEBUGFILENAME => 'log/debug/RubeGDebug'.LOCALTIME.".txt";

my $timeout = 0;
my $maxTimeout = 15;
my $reset = 120;
my $alarm = 1;
my $heartbeat = 0;
my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime();
my $date = [$sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst];

sub  new {
    my $class = shift @_;

    my $self = {
        # Here is where we start the server configuration
        port => 42069,
        maxTimeout => $maxTimeout,
        timeout => $timeout = $maxTimeout, # This is the timeout time in seconds
        reset => $reset,
        alarm => $alarm,
        heartbeat => $heartbeat,
        date => $date,
    };
     
    bless ($self, $class);
    return $self;
}


1;