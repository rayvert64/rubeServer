#!/usr/bin/perl
#================================================================--
# File Name    : RubeServer!
#
# Purpose      : A server for a distributed rube goldberg machine!
#
# Author       : Philippe Boutin, Vancouver Island University
#
# System       : Perl (Linux)
#
# TODO         : Make this not a single 500-line monstrosity of 
#                   a file
#
#=================================================================

use warnings;
use strict;

use POSIX;
use threads;
use FileHandle;
use threads::shared;
use Thread::Queue;
use Thread::Semaphore;
use IO::Select;
use IO::Socket;
use Time::HiRes;
use Time::Local;

use lib "./";
use ServerConfiguration;
use Table::RUBEG;
use Record::Rubeg;
use Collection::Line;
use Collection::Queue;
use Collection::Utils;
use Collection::RubeUtils;
use Packet::RubeG;
use Text::ASCIITable;
use Sockets::SocConS;
use Threads::Networking;
use Threads::Dispatcher;
use Threads::Receiver;
use Threads::TTLHandler;

#################################################################################
# Shared variables(locked by semaphore, except with counter, since the value 
# is not mission critical)
#################################################################################
#Config and its clone
# Here is where we start the server configuration located in config.pl
my %criticalVar :shared;
my %criticalVarSms :shared;
my %controlFlowSms :shared;

my $originConfig = ServerConfiguration->new();
$criticalVar{config} = shared_clone($originConfig);
$criticalVarSms{config} = Thread::Semaphore->new();

$criticalVar{registrationtime} = ServerConfiguration::REGISTRATIONTIME;

# Debugging file handler
my constant $debugFileName = ServerConfiguration::DEBUGFILENAME;

$criticalVar{debugFileName} = $debugFileName;

$criticalVarSms{debugFh} = Thread::Semaphore->new();

# counter for heartbeat
$criticalVar{counter} = 0;

# Who is next in turn and do we skip the next in line
$criticalVar{next} = 0;
$criticalVar{skip} = 0;
$criticalVarSms{nextSkip} = Thread::Semaphore->new();

# We don't need semaphores for these since we should never be writing to
#    these as we are writing to them.
$criticalVar{startTime} = 0;
$criticalVar{endTime} = 0;

# Set up the Ruth table
my $originTable = Table::RUBEG->new();
$criticalVar{table} = shared_clone($originTable);
$criticalVarSms{table} = Thread::Semaphore->new();

# Set up first line module
my $originLine1 = Collection::Line->new();
$criticalVar{line} = shared_clone($originLine1);
$criticalVarSms{line} = Thread::Semaphore->new();

# Set up Queue for the dispatcher
my $originQueue = Collection::Queue->new();
$criticalVar{queue} = shared_clone($originQueue);
$criticalVarSms{queue} = Thread::Semaphore->new();


# Use a semaphore to keep Thread 3 asleep on start and before starting
$controlFlowSms{receiver} = Thread::Semaphore->new(0);
$controlFlowSms{dispatcher} = Thread::Semaphore->new(0);
$controlFlowSms{start} = Thread::Semaphore->new(0);

# Use a semaphore to run the time updater for the ruth table every second
$controlFlowSms{ttl} = Thread::Semaphore->new(0);

# Variable that checks wether we are ready to quit
$controlFlowSms{quit} = 0;
#################################################################################
# Shared variables(locked by semaphore, except with counter, since the value 
# is not mission critical)
#################################################################################

# Initialize debug file
my $debugFh = FileHandle->new();
$debugFh->open("> ".ServerConfiguration::DEBUGFILENAME);
print $debugFh "Debugging - Rube Goldberg Server\nDate: ".ServerConfiguration::LOCALTIME."\n\n";
$debugFh->close();

local $SIG{INT} = sub { leaveScript(); };

# At worst case this takes a full second to run due to the wait semaphore, in this 
# case we do not update the timeout timer. But since the only other block for this
# occurs when we want to reset the timeout it's decnt enough.
# That being said this will run every "more than one second..." so there will be a
#   bit of a scew... but since this isn't a real-time system it's "fine."
#   "Not great, not terrible."
#       - Anatoly Dyatlov(In the HBO mini-series)
#         *Former* Deputy Chief-Engineer of the Chernobyl Nuclear Power Plant
local $SIG{ALRM} = sub {
    if ($criticalVar{config}->{reset} != 0) {
        $criticalVar{config}->{timeout} = ($criticalVar{config}->{timeout})-1;
    }
    $criticalVarSms{config}->up();

    $criticalVar{registrationtime} -= 1;

    if($controlFlowSms{quit} || ($criticalVar{registrationtime} <= 0 && $criticalVar{table}->get_max() == 0)) {
        leaveScript();
    }

    # This is to start the ttl handler which we do every second
    $controlFlowSms{ttl}->up();

    $criticalVar{counter}++;

};

Time::HiRes::ualarm(1_000_000, 1_000_000);


# First thread handles tcp connection
my $networking = threads->create(
    sub {
        # Thread 'cancellation' signal handler
        $SIG{'TERM'} = sub { threads->exit(); };
        Threads::Networking::thread1(\%criticalVar, \%criticalVarSms);
    }
);


# Second thread handles packet processing
my $receiver = threads->create(
    sub {
        # Thread 'cancellation' signal handler
        $SIG{'TERM'} = sub { close($debugFh); threads->exit(); };
        Threads::Receiver::thread1(\%criticalVar, \%criticalVarSms, \%controlFlowSms);
    }
);


# This thread will be what sends the START message to the next rube goldberg machine and
#   will instruct us to stop
my $dispatcher = threads->create(
    sub {
        # Thread 'cancellation' signal handler
        $SIG{'TERM'} = sub { threads->exit(); };
        Threads::Dispatcher::thread1(\%criticalVar, \%criticalVarSms, \%controlFlowSms);
    }
);

# This thread updates the ttl values in the table
my $timeToLiveHandler = threads->create(
    sub {
        # Thread 'cancellation' signal handler
        $SIG{'TERM'} = sub { threads->exit(); };
        Threads::TTLHandler::thread1(\%criticalVar, \%criticalVarSms, \%controlFlowSms);
    }
);


sub leaveScript {
    my $ev = shift @_;
    
    # Send TERM signals to all threads
    print("\nTerminating Listener\n");
    $networking->kill("TERM");
    print("Terminating Receiver\n");
    $receiver->kill("TERM");
    print("Terminating Dispatcher\n");
    $dispatcher->kill("TERM");
    print("Terminating TTL Handler\n");
    $timeToLiveHandler->kill("TERM");

    # Need to unlock all Semaphores to allow all threads to terminate
    $criticalVarSms{config}->up(10000000);
    $criticalVarSms{nextSkip}->up(10000000);
    $controlFlowSms{dispatcher}->up(10000000);
    $controlFlowSms{start}->up(10000000);
    $criticalVarSms{table}->up(10000000);
    $criticalVarSms{line}->up(10000000);
    $criticalVarSms{queue}->up(10000000);
    $controlFlowSms{ttl}->up(10000000);

    # Make sure all threads are joined
    print("\nJoining Listener\n");
    $networking->join();
    print("Joining Receiver\n");
    $receiver->join();
    print("Joining Dispatcher\n");
    $dispatcher->join();
    print("Joining TTL Handler\n");
    $timeToLiveHandler->join();

    # Close debug file
    close($debugFh);

    print("\nShut it down, shut it all down!!!!! \n");
    if ( defined($ev) ) {
        exit($ev);
    }
    else {
        exit(0);
    }
}

while (1) {
    wait();
}

1;